#include<iostream>
#include<string>

template<class T>
class Vector {
public:
	virtual T Get(int i) = 0;
	virtual void Set(int i, T value) = 0;
	virtual int Size() = 0;
	virtual int Capacity() = 0;
	virtual void PushBack(const T& element) = 0;
	virtual void PopBack() = 0;
	virtual void Clear() = 0;
	virtual void Reserve(int n) = 0;
};

template<class T>
class DeepCopyVector : public Vector<T> {
public:
	explicit DeepCopyVector(unsigned int length = 0, const T& default_value = T()) : capacity_(length), size_(length) { 
		arr_ = new T [capacity_];
        for (int i = 0; i < size_; ++i) {
            arr_[i] = default_value;
        }
	}

	DeepCopyVector(const std::initializer_list<T> &list) : capacity_(2*list.size()), size_(list.size()) {
		arr_ = new T [capacity_];
		int count = 0;
		for (auto &element : list) {
			arr_[count] = element;
			++count;
		}
	}

	DeepCopyVector(const DeepCopyVector& other){
        size_ = other.size_;
        capacity_ = other.capacity_;
        arr_ = new T [capacity_];
        for (int i = 0; i < size_; ++i) {
            arr_[i] = other.arr_[i];
        }
    }

    DeepCopyVector(DeepCopyVector&& other) noexcept {
    	size_ = other.size_;
    	capacity_ = other.capacity_;
    	arr_ = other.arr_;
    	other.size_ = 0;
    	other.capacity_ = 0;
        other.arr_ = nullptr;
    }

    DeepCopyVector& operator=(const DeepCopyVector& other) {
        if (&other == this) {
            return *this;
        }
        delete [] arr_;
        size_ = other.size_;
        capacity_ = other.capacity_;
        arr_ = new T [capacity_];
        for (int i = 0; i < size_; ++i) {
            arr_[i] = other.arr_[i];
        }
        return *this;
    }

    DeepCopyVector& operator=(DeepCopyVector&& other) noexcept {
        if (&other == this) {
            return *this;
        }
        size_ = other.size_;
        capacity_ = other.capacity_;
        delete [] arr_;
        arr_ = other.arr_;
        other.arr_ = nullptr;
        other.size_ = 0;
        other.capacity_ = 0;
        return *this;
    }

	friend std::ostream& operator<<(std::ostream& dest, const DeepCopyVector& vector) { 
        std::cout << std::endl;
        for (int i = 0; i < vector.size_; ++i) {
            std::cout << vector.arr_[i] << " ";
        }
        std::cout << std::endl << "size: " << vector.size_ << std::endl << "capacity: " << vector.capacity_ << std::endl;
        
        return dest;
    }

    T Get(int i) override {
    	if (i >= size_ || i < 0) {
            throw std::runtime_error("Invalid index received");
        }
        return arr_[i];
    }

    void Set(int i, T value) override {
    	if (i >= size_ || i < 0) {
            throw std::runtime_error("Invalid index received");
        }
        arr_[i] = value;
    }

    int Size() override {
    	return size_;
    }

    int Capacity() override {
    	return capacity_;
    }

    void PushBack(const T& element) override {
        if (capacity_ == size_) {
            T *buffer_arr = new T [capacity_];
            for (int i = 0; i < size_; ++i) {
                buffer_arr[i] = arr_[i];
            }
            delete [] arr_;
            capacity_ *= 2;
            arr_ = new T [capacity_];
            for (int i = 0; i < size_; ++i) {
                arr_[i] = buffer_arr[i];
            }
            delete [] buffer_arr;
        }
        size_ ++;
        arr_[size_ - 1] = element;
    }    

    void PopBack() override {
        size_ --;
        if (size_ <= capacity_ / 4) {
            T *buffer_arr = new T [capacity_ / 4 + 1];
            for (int i = 0; i < size_; ++i) {
                buffer_arr[i] = arr_[i];
            }
            delete [] arr_;
            capacity_ /= 2;
            arr_ = new T [capacity_];
            for (int i = 0; i < size_; ++i) {
                arr_[i] = buffer_arr[i];
            }
            delete [] buffer_arr;
        }
    }

    void Clear() override {
    	size_ = 0;
    	delete [] arr_;
    	arr_ = new T [capacity_];
    }

    void Swap(DeepCopyVector<T> &other) {
    	DeepCopyVector<T> boof = other;
    	other = *this;
    	*this = boof;
    }

    void Reserve(int n) override {
    	if (n > capacity_) {
    		DeepCopyVector<T> boof = *this;
    		delete [] arr_;
    		capacity_ = n;
    		arr_ = new T [capacity_];
    		for (int i = 0; i < size_; ++i) {
    			arr_[i] = boof.arr_[i];
    		}
    	}
    }

    DeepCopyVector& operator+=(const DeepCopyVector<T>& other) {
        DeepCopyVector<T> boof = *this;
        delete [] arr_;
        capacity_ += other.capacity_;
        size_ += other.size_;
        arr_ = new T [capacity_];
        for (int i = 0; i < size_; ++i) {
        	if (i < boof.size_) {
        		arr_[i] = boof.arr_[i];
        	} else {
        		arr_[i] = other.arr_[i - boof.size_];
        	}
        }        
        return *this;
    }

    friend DeepCopyVector operator+(DeepCopyVector<T> vec_1, const DeepCopyVector<T>& vec_2) {
        vec_1 += vec_2;
        return vec_1;
    }

    DeepCopyVector& operator*=(const int& n){
    	DeepCopyVector<T> boof = *this;
        for (int i = 0; i < n-1; ++i){
            *this += boof;
        }
        return *this;
    }

    friend DeepCopyVector operator*(DeepCopyVector<T> vec_1, const int& n){
        vec_1 *= n;
        return vec_1;
    }

    friend DeepCopyVector operator*(const int& n, DeepCopyVector<T> vec_2){
        vec_2 *= n;
        return vec_2;
    }

    ~DeepCopyVector() {
        delete [] arr_; 
    } 
private:
	int capacity_;
    int size_;
    T *arr_;
};

template<class T>
class ShallowCopyVector : public Vector<T> {
public:
	explicit ShallowCopyVector(unsigned int length = 0, const T& default_value = T()) : capacity_(length), size_(length) { 
        deep_content_ = new DeepCopyVector<T>(length, default_value);
        for (int i = 0; i < size_; ++i) {
        	deep_content_->Set(i, default_value);
        }
        pointers_num_++;
	}

	ShallowCopyVector(const std::initializer_list<T> &list) : capacity_(2*list.size()), size_(list.size()) {
		deep_content_ = new DeepCopyVector<T>(size_);
		int count = 0;
		for (auto &element : list) {
			deep_content_->Set(count, element);
			++count;
		}
		pointers_num_++;
	}

	ShallowCopyVector(const ShallowCopyVector& other){
        deep_content_ = other.deep_content_;
        pointers_num_++;
    }

    ShallowCopyVector(ShallowCopyVector&& other) noexcept {
    	deep_content_ = other.deep_content_;
    	other.deep_content_ = nullptr;
    }

    ShallowCopyVector& operator=(const ShallowCopyVector& other) {
        if (&other == this) {
            return *this;
        }
        deep_content_ = other.deep_content_;
        pointers_num_++;
        return *this;
    }

    ShallowCopyVector& operator=(ShallowCopyVector&& other) noexcept {
        if (&other == this) {
            return *this;
        }
        deep_content_ = other.deep_content_;
        other.deep_content_ = nullptr;
        return *this;
    }

	T Get(int i) override {
		return deep_content_->Get(i);
	}

	void Set(int i, T value) override {
		deep_content_->Set(i, value);
	}

	int Size() override {
		return deep_content_->Size();
	}

	int Capacity() override {
		return deep_content_->Capacity();
	}

	void PushBack(const T& element) override {
		deep_content_->PushBack(element);
	}
	
	void PopBack() override {
		deep_content_->PopBack();
	}

	void Clear() override {
		deep_content_->Clear();
	}

	void Reserve(int n) override {
		deep_content_->Reserve(n);
	}

	void Swap(ShallowCopyVector<T> &other) {
		DeepCopyVector<T>* boof = other.deep_content_;
		other.deep_content_ = this->deep_content_;
		this->deep_content_ = boof;
	}

	friend std::ostream& operator<<(std::ostream& dest, const ShallowCopyVector& vector) { 
        std::cout << *vector.deep_content_;
        
        return dest;
    }

    ~ShallowCopyVector() {
    	if (pointers_num_ == 1) {
    		//deep_content_->~DeepCopyVector();
    		delete deep_content_;
    	} else {
    		pointers_num_--;
    	}
    }
private:
	DeepCopyVector<T>* deep_content_; 
	int pointers_num_ = 0;
	int capacity_;
    int size_;
};

template<class T>
class COWVector : public Vector<T> {
public:
	using Vector<T>::Vector;
private:
	int capacity_;
    int size_;
    T *arr_;
};

int main() {
	DeepCopyVector<float> dcv {1.24, 12.323, 13.23};
	DeepCopyVector<float> dcv1 {-123, 123, 932, 12.234, -42.123};
	DeepCopyVector<float> dcv2;
	std::cout << dcv << dcv1 << std::endl;
	dcv1.Swap(dcv);
	std::cout << "\n---Swap---" << dcv << dcv1 << std::endl;

	dcv2 = 4 * dcv1;
	std::cout << dcv2 << std::endl;

	ShallowCopyVector<int> scv {2, 54, -12, 34};
	ShallowCopyVector<int> scv1 {5, 2};
	std::cout << scv << scv1 << std::endl;
	//scv.Swap(scv1);
	std::cout << "\n---Swap---" << scv << scv1 << std::endl;

    return 0;
}

