#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>

using Vertex = int;

enum class Method {
	mListOfRibs,
	mAdjacencyList,
	mAdjacencyMatrix
};

enum class NodeState {
	kNotSeen, 
	kSeen, 
	kVisited
};

class Graph {
public:
	explicit Graph(int vertices_num) : 
        vertices_num_(vertices_num), edges_(vertices_num), all_vertices_(vertices_num), 
        	states_(vertices_num) {
            for (int i = 0; i < vertices_num; ++i) {
                all_vertices_[i] = i;
                states_[i] = NodeState::kNotSeen;
            }
        };

    void AddEdge(Vertex from, Vertex to) {
    	if (std::find(edges_[from].begin(), edges_[from].end(), to) == edges_[from].end()) {
    		edges_[from].push_back(to);
    	}
    };

    const std::vector<Vertex>& GetAllVertices() {
        return all_vertices_;
    };

    const std::vector<Vertex>& GetAdjacentVertices(Vertex vertex) {
        return edges_[vertex];
    };


  	void Read(const Method & method) {
  		if (method == Method::mListOfRibs) {
  			std::cout << "Enter the number of ribs:\n";
  			std::cin >> edges_num_;
  			for (int i = 0; i < edges_num_; ++i) {
  				Vertex vertex_from, vertex_to;
  				std::cin >> vertex_from >> vertex_to;
  				AddEdge(vertex_from, vertex_to);
  			}
  		}
  		else if (method == Method::mAdjacencyList) {
  			std::cout << "Enter the vertex_from, then the number of edges\n";  
  			std::cout << "outgoing from this vertex and then the vertices_to:\n";
  			for (int i = 0; i < vertices_num_; ++i) {
  				int number_of_adjacent_vertices;
  				Vertex vertex_from;
  				std::cin >> vertex_from >> number_of_adjacent_vertices;
  				for (int j = 0; j < number_of_adjacent_vertices; ++j) {
  					Vertex vertex_to;
  					std::cin >> vertex_to;
  					AddEdge(vertex_from, vertex_to);
  				}
  			}
  		}
  		else if (method == Method::mAdjacencyMatrix) {
  			std::cout << "Enter adjacency matrix:\n";
  			for (int i = 0; i < vertices_num_; ++i) {
  				for (int j = 0; j < vertices_num_; ++j) {
  					bool is_edge;
  					std::cin >> is_edge;
  					if (is_edge) {
  						AddEdge(i, j);
  					}
  				}
  			}
  		}
  	};

  	Graph GetGraph(Graph graph_return) {
  		return graph_return;
  	}

  	void TopologicalSort() {
  		bool is_top_sorted = true;
  		for (Vertex vertex : all_vertices_) {
  			if (states_[vertex] == NodeState::kNotSeen) {
  				is_top_sorted = DFS(vertex, is_top_sorted);
  			}
  		}
  		if (is_top_sorted) {
  			std::cout << "\nAfter Topological sorting\n";
	    	while (TopSorted_.empty() != true) {
	    		std::cout << TopSorted_.top();
	    		TopSorted_.pop();
    		}
    		std::cout << "\n";
    	} else {
    		std::cout << "\nLoop found. Topological sorting is not possible\n";
    	}
  	}

  	bool DFS(Vertex vertex, bool is_top_sorted) {
  		if (is_top_sorted) {
		    if (states_[vertex] == NodeState::kNotSeen) {
		        states_[vertex] = NodeState::kSeen;
		        for (Vertex next : edges_[vertex]){
		      	 	is_top_sorted = DFS(next, is_top_sorted);
		        }
	    		states_[vertex] = NodeState::kVisited;
	    		TopSorted_.push(vertex);
		    }
		    if (states_[vertex] == NodeState::kSeen) {
		       	is_top_sorted = false;
		    }
	    }
	    return is_top_sorted;
	}

private:
	int vertices_num_, edges_num_;
    std::vector<std::vector<Vertex>> edges_;
    std::vector<Vertex> all_vertices_;
    std::vector<NodeState> states_;
    std::stack<Vertex> TopSorted_;
};

int main() {
	Method method;
	int vertices_num;
	std::cout << "Enter the number of vertices in the graph:\n";
	std::cin >> vertices_num;
	Graph graph(vertices_num);

	method = Method::mListOfRibs;
	//method = Method::mAdjacencyList;
	//method = Method::mAdjacencyMatrix;
	graph.Read(method);
	graph.TopologicalSort();

	std::cout << "\nRead graph adjacency list:\n";
	for (int i = 0; i < vertices_num; ++i) {
		for (int j = 0; j < graph.GetAdjacentVertices(i).size(); ++j) {
			std::cout << graph.GetAdjacentVertices(i)[j] << " ";
		}
		std::cout << "\n";
	}

	return 0;
}

