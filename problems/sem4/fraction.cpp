#include <iostream>
#include <stdexcept> 

int GCD(int num_1, int num_2) {
    if (num_1 % num_2 == 0) {
        return num_2;
    }
    if (num_2 % num_1 == 0) {
        return num_1;
    }
    if (num_1 > num_2) {
        return GCD(num_1 % num_2, num_2);
    }
    return GCD(num_1, num_2 % num_1);
};

int LCM(int num_1, int num_2) {
    return (num_1 * num_2) / GCD(num_1, num_2);
};

class Fraction {
public:
	explicit Fraction(int numerator = 0, int denominator = 1) {
		if (denominator == 0) {
            throw std::runtime_error("Division by zero!");
        }
        if (denominator < 0) {
			numerator = -numerator;
			denominator = -denominator;
		}
        int gcd = GCD(abs(numerator), denominator);
        numerator_ = numerator / gcd;
        denominator_ = denominator / gcd;
	};
    
	friend std::ostream& operator<<(std::ostream& dest, const Fraction& frac) { 
        dest << frac.numerator_ << "/" << frac.denominator_ << std::endl;
        return dest;
    };

    friend Fraction operator-(Fraction& frac) {
        frac.numerator_ = -frac.numerator_;
        return frac;
    };

    Fraction& operator+=(const Fraction& other) {
        int lcm = LCM(denominator_, other.denominator_);
        numerator_ = numerator_ * (lcm / denominator_) + other.numerator_ * (lcm / other.denominator_);
        
        int gcd = GCD(abs(numerator_), lcm);
        numerator_ = numerator_ / gcd;
        denominator_ = lcm / gcd;       
        
        return *this;
    }
    
    friend Fraction operator+(Fraction frac_1, const Fraction& frac_2) {
        frac_1 += frac_2;
        return frac_1;
    }
    
    Fraction& operator-=(const Fraction& other) {        
        int lcm = LCM(denominator_, other.denominator_);
        numerator_ = numerator_ * (lcm / denominator_) - other.numerator_ * (lcm / other.denominator_);
         
        int gcd = GCD(abs(numerator_), lcm);
        numerator_ = numerator_ / gcd;
        denominator_ = lcm / gcd;       

        return *this;
    }

    friend Fraction operator-(Fraction frac_1, const Fraction& frac_2) {
        frac_1 -= frac_2;
        return frac_1;
    }
    
    Fraction& operator*=(const Fraction& other) {
        numerator_ = numerator_ * other.numerator_;
        denominator_ = denominator_ * other.denominator_;
        
        int gcd = GCD(abs(numerator_), denominator_);
        numerator_ = numerator_ / gcd;
        denominator_ = denominator_ / gcd;   
        
        return *this;
    }    
    
    friend Fraction operator*(Fraction frac_1, const Fraction& frac_2) {
        frac_1 *= frac_2;
        return frac_1;
    }

    Fraction& operator/=(Fraction other) {
        if (other.numerator_ == 0) {
            throw std::runtime_error("Division by zero!");
        }
        int buff = other.denominator_;
        other.denominator_ = other.numerator_;
        other.numerator_ = buff;
 
        if (other.denominator_ < 0) {
			other.numerator_ = -other.numerator_;
			other.denominator_ = -other.denominator_;
		}
        *this *= other;
        
        return *this;
    }

    friend Fraction operator/(Fraction frac_1, const Fraction& frac_2) {
        frac_1 /= frac_2;
        return frac_1;
    }
    
    friend bool operator==(const Fraction& frac_1, const Fraction& frac_2) {
        if (frac_1.numerator_ == frac_2.numerator_ && frac_1.denominator_ == frac_2.denominator_) {
            return true;
        } else return false;
    }
    
    friend bool operator!=(const Fraction& frac_1, const Fraction& frac_2) {
        return !(frac_1 == frac_2);
    }

    friend bool operator<(Fraction frac_1, Fraction frac_2) {
	    int lcm = LCM(frac_1.denominator_, frac_2.denominator_);
	    frac_1.numerator_ = frac_1.numerator_ * (lcm / frac_1.denominator_);
	    frac_2.numerator_ = frac_2.numerator_ * (lcm / frac_2.denominator_);
	    if (frac_1.numerator_ < frac_2.numerator_) {
	    	return true;
	    } else return false;
	}

	friend bool operator>(Fraction frac_1, Fraction frac_2) {
		int lcm = LCM(frac_1.denominator_, frac_2.denominator_);
	    frac_1.numerator_ = frac_1.numerator_ * (lcm / frac_1.denominator_);
	    frac_2.numerator_ = frac_2.numerator_ * (lcm / frac_2.denominator_);
	    if (frac_1.numerator_ > frac_2.numerator_) {
	    	return true;
	    } else return false;
	}

	friend bool operator<=(Fraction frac_1, Fraction frac_2) {
		int lcm = LCM(frac_1.denominator_, frac_2.denominator_);
	    frac_1.numerator_ = frac_1.numerator_ * (lcm / frac_1.denominator_);
	    frac_2.numerator_ = frac_2.numerator_ * (lcm / frac_2.denominator_);
	    if (frac_1.numerator_ <= frac_2.numerator_) {
	    	return true;
	    } else return false;
	}

	friend bool operator>=(Fraction frac_1, Fraction frac_2) {
		int lcm = LCM(frac_1.denominator_, frac_2.denominator_);
	    frac_1.numerator_ = frac_1.numerator_ * (lcm / frac_1.denominator_);
	    frac_2.numerator_ = frac_2.numerator_ * (lcm / frac_2.denominator_);
	    if (frac_1.numerator_ >= frac_2.numerator_) {
	    	return true;
	    } else return false;
	}
 
private:
	int numerator_, denominator_;
};

int main()
{	
	int n1, n2, d1, d2;
	std::cout << "Enter the numerator and denominator for frac 1:\n";
	std::cin >> n1 >> d1;
	std::cout << "Enter the numerator and denominator for frac 2:\n";
	std::cin >> n2 >> d2;
	
	Fraction frac1(n1, d1), frac2(n2, d2);
	std::cout << std::endl << frac1 << frac2 << std::endl;

    Fraction other_frac(129, 212);     
    Fraction frac4 = -other_frac;
    std::cout << frac4 << std::endl;

    frac4 = frac1 + frac2;
    std::cout << " + -- " << frac4;
    frac4 = frac1 - frac2;
    std::cout << " - -- " << frac4;
    frac4 = frac1 * frac2;
    std::cout << " * -- " << frac4;
    frac4 = frac1 / frac2;
    std::cout << " / -- " << frac4 << std::endl;
    
    if (frac1 == frac2) { 
        std::cout << "frac1 == frac2 is true\n";
    } else {
        std::cout << "frac1 == frac2 is false\n";
    }
    if (frac1 != frac2) { 
        std::cout << "frac1 != frac2 is true\n";
    } else {
        std::cout << "frac1 != frac2 is false\n";
    }
    if (frac1 < frac2) { 
        std::cout << "frac1 < frac2 is true\n";
    } else {
        std::cout << "frac1 < frac2 is false\n";
    }
    if (frac1 > frac2) { 
        std::cout << "frac1 > frac2 is true\n";
    } else {
        std::cout << "frac1 > frac2 is false\n";
    }
    if (frac1 <= frac2) { 
        std::cout << "frac1 <= frac2 is true\n";
    } else {
        std::cout << "frac1 <= frac2 is false\n";
    }
    if (frac1 >= frac2) { 
        std::cout << "frac1 >= frac2 is true\n";
    } else {
        std::cout << "frac1 >= frac2 is false\n";
    }

	return 0;
}

